import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  Button,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const SigninScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        source={require('../assets/images/bck.jpg')}
        style={styles.image}>
        <View style={{flex: 1, backgroundColor: 'rgba(255,251,251,0.7)'}}>
          <ScrollView>
            <View
              style={{marginTop: 70, marginBottom: 80, alignItems: 'center'}}>
              {/* <Image
                source={require('./assets/Images/logo.png')}
                style={{width: 50, height: 50}}
              /> */}
            </View>

            <View style={{margin: 12}}>
              <View style={{flexDirection: 'column'}}>
                <Text style={styles.user_pass}> EMAIL</Text>
                <TextInput style={styles.user_pass_line} placeholder="" />
                <Text style={styles.user_pass}> PASSWORD</Text>
                <TextInput style={styles.user_pass_line} placeholder="" />
              </View>

              <Text
                style={{
                  color: '#6B0103',
                  textAlign: 'right',
                  marginEnd: 15,
                  paddingTop: 10,
                }}>
                Forgot Password ?
              </Text>
            </View>
            <View style={{flexDirection: 'column', margin: 15}}>
              <View style={styles.SubmitButton}>
                <TouchableOpacity onPress={() => navigation.navigate('home')}>
                  <Text style={styles.SubmitButtonText}> Log In </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 10,
                }}>
                <Text>Dont Have an account?</Text>
                <TouchableOpacity onPress={() => navigation.navigate('signup')}>
                  <Text style={{color: '#6B0103'}}>Signup</Text>
                </TouchableOpacity>
              </View>
              <Text
                style={{textAlign: 'center', paddingTop: 30, color: '#F03C02'}}>
                Or Login With
              </Text>
            </View>
          </ScrollView>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FEFEFE',
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  user_pass: {
    padding: 15,
    color: '#F03C02',
  },
  user_pass_line: {
    marginVertical: 8,
    marginHorizontal: 15,
    justifyContent: 'flex-start',
    fontSize: 15,
    borderBottomWidth: 1,
    marginHorizontal: 15,
    borderBottomColor: '#C21A01',
  },
  SubmitButton: {
    marginTop: 40,
    paddingVertical: 10,
    borderRadius: 8,
    width: wp('50'),
    height: hp('8'),
    backgroundColor: '#6B0103',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  SubmitButtonText: {
    color: '#F03C02',
    textAlign: 'center',

    fontSize: 20,
    fontFamily: 'AnticSlab-Regular',
  },
});

export default SigninScreen;
