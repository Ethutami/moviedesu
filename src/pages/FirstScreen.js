import React, {useState} from 'react';

import {
  View,
  Text,
  Image,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Pressable,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const FirstScreen = ({route, navigation}) => {
  const [btnLogin, setBtnLogin] = useState(false);
  const [btnSignup, setBtnSignup] = useState(false);
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../assets/images/bck.jpg')}
        style={styles.background_Style}>
        <View style={styles.background_rgb_Style}>
          <View style={{justifyContent: 'center', marginTop: 80}}>
            <View style={{alignItems: 'center'}}>
              <Image
                //source={require('./assets/Images/movielogo.png')}
                style={{width: wp('50'), height: hp('12')}}
              />
            </View>

            <View style={{alignItems: 'center', marginTop: 80}}>
              <Pressable //login
                onPressIn={() => setBtnLogin(true)}
                onPressOut={() => {
                  setBtnLogin(false);
                  navigation.navigate('login');
                }}>
                <View
                  style={
                    btnLogin ? styles.SubmitButton : styles.SubmitButton_outline
                  }>
                  <Text
                    style={
                      btnLogin
                        ? styles.SubmitButtonText2
                        : styles.SubmitButtonText
                    }>
                    LOGIN
                  </Text>
                </View>
              </Pressable>
              <Pressable //signUp
                onPressIn={() => setBtnSignup(true)}
                onPressOut={() => {
                  setBtnSignup(false);
                  navigation.navigate('signup');
                }}>
                <View
                  style={
                    btnSignup
                      ? styles.SubmitButton
                      : styles.SubmitButton_outline
                  }>
                  <Text
                    style={
                      btnSignup
                        ? styles.SubmitButtonText2
                        : styles.SubmitButtonText
                    }>
                    SIGN UP
                  </Text>
                </View>
              </Pressable>
            </View>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background_Style: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  background_rgb_Style: {
    flex: 1,
    backgroundColor: 'rgba(50,5,30,0.8)',
  },
  SubmitButton: {
    marginTop: 30,
    paddingVertical: 10,
    borderRadius: 8,
    width: wp('50'),
    height: hp('8'),
    backgroundColor: '#6B0103',
    justifyContent: 'center',
    alignItems: 'center',
  },
  SubmitButton_outline: {
    marginTop: 30,
    paddingVertical: 10,
    borderRadius: 8,
    width: wp('50'),
    height: hp('8'),
    borderColor: '#F03C02',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  SubmitButtonText: {
    color: '#6B0103',
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'AnticSlab-Regular',
  },
  SubmitButtonText2: {
    color: '#F03C02',
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'AnticSlab-Regular',
  },
});

export default FirstScreen;
