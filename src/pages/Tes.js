import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Icon_menu from 'react-native-vector-icons/Entypo';
import Icon_search from 'react-native-vector-icons/EvilIcons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Axios from 'axios';
import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import {request_data} from '../reduxThunk/action';

const TesMovie = ({navigation}) => {
  const popular = useSelector(state => state.MovieReducer.popular);
  const topRated = useSelector(state => state.MovieReducer.top_rated);

  const dispatch = useDispatch();
  //   useEffect(() => {
  //     dispatch(request_data('top_rated'));
  //     console.log('liat dong toprated', topRated);
  //   }, []);

  useEffect(() => {
    dispatch(request_data('popular'));
    dispatch(request_data('top_rated'));
    // console.log('liat dong popular', popular);
    // console.log('liat dong top Rated', topRated);
  }, []);

  const renderItem = ({item}) => {
    return (
      <View style={{alignItems: 'center'}}>
        <Image
          source={{uri: `https://image.tmdb.org/t/p/w500${item.poster_path}`}}
          style={{width: wp(45), height: hp(45), marginHorizontal: 2}}
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#1C0113'}}>
      <View style={styles.header}>
        <Icon_menu name="menu" size={30} color="#F03C02" />
        <Image
          //source={require('./assets/Images/movie.png')}
          style={{width: wp(60), height: hp(5)}}
        />
        <TouchableOpacity onPress={() => navigation.navigate('Search')}>
          <Icon_search name="search" size={25} color="#F03C02" />
        </TouchableOpacity>
      </View>
      <View style={{margin: 10}}>
        <ScrollView>
          <Text style={styles.selectContent}>TOP RATED</Text>
          <FlatList
            data={topRated}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
          <Text style={styles.selectContent}>POPULAR</Text>
          <FlatList
            data={popular}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'baseline',
    paddingVertical: 10,
    backgroundColor: '#11000C',
  },
  selectContent: {
    fontSize: 20,
    color: '#F03C02',
    paddingVertical: 8,
  },
});

export default TesMovie;
