import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducer';

const midleware = [thunk];
export const store = createStore(reducer, applyMiddleware(...midleware));
