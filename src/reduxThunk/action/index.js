// import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const request_data = data => dispatch => {
  //console.log('helo ini adalah data', data);

  axios
    .get(
      `https://api.themoviedb.org/3/movie/${data}?api_key=3ab89b5f5819504df8e2ea3c5f0e8862&language=en-US&page=1`,
    )
    .then(resp => {
      //console.log('ini', resp.data.results);
      if (data === 'top_rated') {
        return dispatch({
          type: 'REQUEST_DATA_MOVIE_TOP_RATED',
          payload: resp.data.results,
        });
      } else {
        return dispatch({
          type: 'REQUEST_DATA_MOVIE_POPULAR',
          payload: resp.data.results,
        });
      }
    });
};

export const getUserDetailAction1 = () => dispatch => {
  let url = `https://team-c.gabatch11.my.id/user/`;
 
  const AuthStr = 'Bearer '.concat('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwODNkMjkzMTA2ZjkwMDgzNTgwZDNiOSIsImlhdCI6MTYxOTI2NTUyNX0.koBMfo10aVfzY1pM8jgSuNZyXO_ifsJn9pLHACAFHs4');
  axios
    .get(url, {
      headers: {Authorization: AuthStr},
    })
    .then(response => {
      console.log(response.data.data);
    })
    .catch(error => {
      console.log(error.message);
    });
};

