const initialState = {
  top_rated: [],
  popular: [],
};

const MovieReducer = (state = initialState, action) => {
  const {type, payload} = action;
  //console.log('data yang diterima reducer dari action', payload);
  switch (type) {
    case 'REQUEST_DATA_MOVIE_TOP_RATED':
      return {
        ...state,
        top_rated: payload, //{}
      };
    case 'REQUEST_DATA_MOVIE_POPULAR':
      return {
        ...state,
        popular: payload, //{}
      };

    default:
      return state;
  }
};

export default MovieReducer;
