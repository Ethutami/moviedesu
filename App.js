/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Tes from './src/pages/FirstScreen';
import Login from './src/pages/SigninScreen';
import SignUp from './src/pages/SignupScreen';
import Home from './src/pages/Home';
import {Provider} from 'react-redux';
import {store} from './src/reduxThunk';

import TesMovie from './src/pages/Tes';
import Lala from './src/pages/lala'

const App: () => Node = () => {
  const Stack = createStackNavigator();
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="tes" component={Tes} />
          <Stack.Screen name="login" component={Login} />
          <Stack.Screen name="signup" component={SignUp} />
          <Stack.Screen name="home" component={Home} />
        </Stack.Navigator>
      </NavigationContainer> 
      {/* <TesMovie /> */}
      {/* <Lala/> */}
    </Provider>
  );
};

export default App;
